from django_component import Component, Library

register = Library()


@register.component
class Children(Component):
    template = "children.html"


@register.component
class Context(Component):
    template = "context.html"

    def context(self, number):
        return {"number": number, "square": number ** 2}


@register.component
class Media(Component):
    template = "media.html"

    class Media:
        css = {"all": ["media.css"]}
        js = ["media.js"]


@register.component
class MediaExternal(Component):
    template = "media.html"

    class Media:
        css = {"all": ["https://example.com/media.css"]}
        js = ["https://example.com/media.js"]


@register.component
class Prop(Component):
    template = "prop.html"


@register.component
class Arg(Component):
    template = "arg_tag.html"


@register.component
class ArgList(Component):
    template = "arg_tag_list.html"


@register.component
class Nested(Component):
    template = "nested.html"


@register.component
class ContextScope(Component):
    template = "context_scope.html"
